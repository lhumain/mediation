---
title: ""
draft: false
tags: 
date:
---

| Titre :  | {{ $json.Titre }}                        |
| -------- | ---------------------------------------- |
| Statut : | {{ $json.Statut }}                       |
| Dates :  | {{ $json['Dates d\'entrée sortie UR'] }} |
## Biographie
 {{ $json.Biographie }}
## Projets
### En cours
{{ $json['Projets (en cours)'] }}
### Passés
 {{ $json['Projets (passés)'] }}
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | {{ $json.HAL }}      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | {{ $json.LinkedIn }} |
| ![[mail.svg\|20]]                           |  {{ $json.Mail }}    |
