---
title: Bienvenue sur la médiation de l'unité de recherche LHUMAIN
draft: false
tags: 
date: 2024-05-31
---
## Manifeste LHUMAIN

Si les questions de dénomination en linguistique ont toujours été fondamentales, la dénomination d’une nouvelle U.R. s’inscrivant en Sciences du langage avec un engagement interdisciplinaire l’est tout autant. Au-delà des significations particulières portées par chaque élément de l’acronyme _Langage(s), HUmanités, Média-tions, Apprentissages, Interactions, Numérique_, le tout vaut plus que la somme des parties, au sens des Théories de la Complexité et du Chaos. Si chacune des étiquettes à l’origine de l’acronyme recèle sa propre conception, dans le cadre qui est celui des chercheures et des chercheurs travaillant dans notre U.R., la question de l'Être de langage revêt une dimension centrale, à la fois dans notre manière de conduire nos raisonnements intellectuels mais aussi dans notre ancrage épistémologique.

Ainsi, nous ambitionnons de mettre l’Humain au cœur de nos objets de recherche, de nos terrains et de nos objectifs. Nous travaillons dans une dynamique de recherche en action, impliquée, attachée aux activités humaines pour mieux comprendre et éclairer, voire accompagner ou questionner les mutations de nos sociétés.

Accéder à la présentation des lettres du sigle :
- [[1-L|L - Langages]]
- [[2-HU|Hu - Humanités]]
- [[3-M|M - Média-tions]]
- [[4-A|A - Apprentissages]]
- [[5-I|I - Interactions]]
- [[6-N|N - Numérique]]
## Liens utiles :
- Lien HAL
- Lien vers le site
