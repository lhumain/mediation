---
title: "Chantal CHARNET"
draft: false
tags: 
  - A
  - I
  - N
date: ""
---

| Titre :  | MCF émérite                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 - 2022 |
## Biographie
 ### Parcours académique
Chantal Charnet a été professeure des universités en sciences du langage à l'Université Paul Valéry Montpellier 3 de 2005 à 2019, où elle a notamment créé et dirigé le master Sciences du langage spécialité apprentissages, formation ouverte et à distance de 2004 à 2010.[^1] Depuis 2020, elle est professeure des universités émérite.[^1]

### Domaines de recherche
Ses thématiques de recherche portent sur la médiation et la culture numérique, l'appropriation des savoirs en ligne, l'innovation pédagogique et les pratiques techno-pédagogiques dans la formation en ligne et à distance.[^1] Elle s'intéresse particulièrement à l'étude des formations et médiations numériques.[^1]

### Responsabilités et projets
Chantal Charnet a été responsable de conventions avec diverses universités étrangères comme l'Université de Tbilissi (Géorgie), l'Université de Sanaa (Yémen) et la Syrian Virtual University (Damas, Syrie).[^1] Elle a également été responsable scientifique du projet PREFALC visant à promouvoir la formation et la recherche en humanités numériques en Colombie, au Pérou et en France de 2018 à 2019.[^1]

### Publications
Elle est l'auteure de l'ouvrage "Comment réaliser une formation ou un enseignement numérique à distance ?" publié aux éditions De Boeck Supérieur.4]5]

Citations:
[^1] https://cv.hal.science/chantal-charnet
[^2] https://lhumain.www.univ-montp3.fr/fr/charnet-chantal
[^3] https://www.lairedu.fr/personne/chantal-charnet/
[^4] https://www.renaud-bray.com/Livres\_Produit.aspx?id=2779549
[^5] https://www.deboecksuperieur.com/auteur/chantal-charnet

## Projets
### En cours
À compléter.
### Passés
 - Partenaire français dans le cadre du projet franco-marocain avec l’Université Mohammed Premier d’Ougda : Mooc ([Passeport pour une recherche active et construite d'informations](https://www.mun.ma/courses/course-v1:UMP+UMP002+session01/about)  (2018-2019) - accessible sur la plateforme MUN (Maroc Université numérique) -) diffusion mars 2021.
- [SPOC (Small Private Offline Courses in prison)](https://lhumain.www.univ-montp3.fr/fr/programmes-europ%C3%A9ens-et-internationaux) - 2019-2022 : participant
- [Le centre 15 à l'ère du numérique](https://lhumain.www.univ-montp3.fr/fr/transfert-et-innovation) : 2018-2022 : participante
- PREFALC (2022-2023) - Colombie, Brésil, France : participante
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Charnet](https://cv.archives-ouvertes.fr/chantal-charnet)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/chantal-charnet-6ba779105?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAABqh264BLGKYZCHAGVALrg40x2WC6-vqACU&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BNZFXuSxnTs%2BLVwe16zPqtg%3D%3D |
| ![[mail.svg\|20]]                           |  chantal.charnet@univ-montp3.fr    |
