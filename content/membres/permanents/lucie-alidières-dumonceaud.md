---
title: "Lucie ALIDIÈRES-DUMONCEAUD"
draft: false
tags: 
  - HU
  - M
  - I
  - N
date: ""
---

| Titre :  | MCF                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 |
## Biographie
 Lucie Alidières-Dumonceaud est une enseignante-chercheuse en sciences du langage à l'Université Paul-Valéry Montpellier 3. Elle est maître de conférences et ses thématiques de recherche incluent la médiation numérique, la linguistique interactionnelle et l'analyse des pratiques professionnelles.[^1][^2][^3]

Alidières-Dumonceaud a obtenu son doctorat de l'Université Paul-Valéry et a poursuivi des recherches postdoctorales au laboratoire Praxiling UMR 5267 dans le cadre du projet ANR CREMM.[^2] Son travail se concentre notamment sur la formation en milieu carcéral, proposant des formats adaptés aux contraintes carcérales comme le projet SPOC IN PRISON.[^4]

Elle a été membre de jurys de thèse et a participé à des congrès sur les publics empêchés, présentant ses recherches sur l'accès à l'éducation pour les personnes incarcérées.[^4][^5]

Citations:
[^1] https://lhumain.www.univ-montp3.fr/fr/alidieres-dumonceaud-lucie
[^2] https://rae.hypotheses.org/lucie-alidieres-dumonceaud
[^3] https://fr.linkedin.com/in/lucie-alidi%C3%A8res-2a30a2a8
[^4] https://www.fied.fr/sites/default/files/2448/poster%20congre%CC%80s%20publics%20empe%CC%82che%CC%81s.pdf
[^5] https://www.theses.fr/172313783

## Projets
### En cours
- SPOC in prison
- IDEFI UM3D - action 13, pédagogie à destination des publics empêchés.
### Passés
 - Co-coordinatrice scientifique du programme SCENOSCOPE (2021 à ce jour) avec [[françois-perea|François Perea]]
- Responsable scientifique du projet Langage Intervention Précoce et Prise en SOIN (LIPPS) - CHU Montpellier pôle psychiatrie Unité  Jeunes Adultes  (2023 à ce jour)
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Alidières](https://halshs.archives-ouvertes.fr/search/index/?q=Lucie+Alidi%C3%A8res&submit=&authFullName_s=Lucie+Alidi%C3%A8res)  - [Alidières-Dumonceaud](https://halshs.archives-ouvertes.fr/search/index/?q=Lucie+Alidi%C3%A8res&submit=&authFullName_s=Lucie+Alidi%C3%A8res-Dumonceaud)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/lucie-alidières-2a30a2a8?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAABbLORsBu-XlwfELyAQMpJJ0CYjh3Fc8xBI&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3Bg205WIEORWCR57RyOXi72g%3D%3D |
| ![[mail.svg\|20]]                           |  lucie.alidieres@univ-montp3.fr    |
