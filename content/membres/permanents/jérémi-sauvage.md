---
title: "Jérémi SAUVAGE"
draft: false
tags: 
  - L
  - HU
  - I
  - N
date: ""
---

| Titre :  | PU                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | depuis 2020 |
## Biographie
 Jérémi Sauvage est un professeur des universités en sciences du langage à l'Université Paul-Valéry Montpellier 3[<sup>1</sup>](https://www.editions-harmattan.fr/index.asp?navig=auteurs&no=9182&obj=artiste)[<sup>2</sup>](https://lhumain.www.univ-montp3.fr/fr/sauvage-j%C3%A9r%C3%A9mi). Il est docteur en sciences du langage et chercheur au laboratoire Dipralang EA 739[<sup>3</sup>](https://booknode.com/auteur/jeremi-sauvage). Ses principaux thèmes de recherche sont les humanités numériques et les processus d'appropriation des langues-cultures, les mathématiques, l'informatique et l'analyse de corpus en acquisition du langage, ainsi que l'articulation entre les langues maternelles et les langues non-maternelles[<sup>4</sup>](https://cv.hal.science/jeremisauvage). 

Sauvage est le porteur du Contrat Plan Etat Région (CPER) "Corpus Humanum" à l'Université Paul-Valéry Montpellier 3 pour 2021-2027[<sup>4</sup>](https://cv.hal.science/jeremisauvage). Il est également rédacteur en chef de la revue scientifique LHUMAINE depuis 2018 et membre du groupe Reliance en Complexité de la Chaire Unesco Edgar Morin[<sup>4</sup>](https://cv.hal.science/jeremisauvage).

Outre ses activités universitaires, Sauvage est également auteur de romans de fantasy publiés aux éditions Fleuve Noir, L'Harmattan et Malpertuis, comme la série "Bleu cobalt"[<sup>1</sup>](https://booknode.com/auteur/jeremi-sauvage) [<sup>5</sup>](https://www.racontemoilaterre.com/livre/9782918809128-bleu-cobalt-tome-1-ailleurs-crepuscule-jeremi-sauvage/).

## Projets
### En cours
- **PEPS (Enseignement du FLE en Présentiel EAD Présentiel et Suivi)**
	Ce projet a pour objectif d'innover d'une point de vue didactique et pédagogique en proposant une formation diplomante hybride en FLE. Porteur : Jérémi Sauvage.
### Passés
  **[BINOGI](https://lhumain.www.univ-montp3.fr/fr/programmes-europ%C3%A9ens-et-internationaux) (Multilingual pedagogies, digital technologies, language learners in Science, Technology, Engineering and Mathematics skills), demande de fond soumis au Centre de Recherche des Sciences Humaines /CRSH, Canada, décembre 2019-mars 2022, 200.000 $CAN)**
	Ce projet vise à développer les compétences en langues et sciences des élèves du primaires et du secondaire via des supports digitaux (vidéos sous-titrés et quizz multilingues) Porteurs pour la France : Jérémi Sauvage et [[nathalie-auger|Nathalie Auger]]

- **[SIRIUS 2.0](http://www.sirius-migrationeducation.org/) (Policy Network on Migrant Education) – ERASMUS + - 1.700.000 € (60.000 € Montpellier) (2017-21)**
	Recherches, échanges et dissémination des études liées au développement des compétences langagières des enfants migrants. Analyses des dispositifs (éducation formelle, informelle) et de l’impact sur les compétences en langues des enfants. Co-porteurs pour la France : Jérémi Sauvage & [[nathalie-auger|Nathalie Auger]]. _Participation Jérémi Sauvage : analyses, supervision de la conception de la plateforme numérique, publications en "digital inclusion"._

- **[LISTIAC](http://grupsderecerca.uab.cat/greip/en/node/680) : Linguistically Sensitive Teachers in All Classrooms, – ERASMUS + KA3 – 2.400.000 € (255.000 € Montpellier) (2018-21)**
	Ce projet porte sur l'analyse des représentations et des pratiques enseignantes face au plurilinguisme. Porteur : University of Abo (Finland), 11 participants dont la France parmi les 4 co-porteurs (avec Gand et Barcelone). Porteur pour la France : [[nathalie-auger|Nathalie Auger]] _Participation Jérémi Sauvage : analyses des données, publications._
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Sauvage](https://cv.hal.science/jeremisauvage)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/jeremi-sauvage-70337170?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAA8A5bMBhW8wo4_RtMCiX6pZp5rd_8exjU4&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BoM2AkdFgRD2fKWGGLy4pIA%3D%3D |
| ![[mail.svg\|20]]                           |  jeremi.sauvage@univ-montp3.fr    |
