---
title: "Laurine DALLE"
draft: false
tags: 
  - L
  - A
  - I
date: ""
---

| Titre :  | MCF                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 |
## Biographie
 Laurine Dalle est une maître de conférences en phonétique et didactique de l'oral en français langue étrangère (FLE) au département des sciences du langage de l'Université Paul-Valéry Montpellier 3\[^2]\[^3][^4]. 

Elle a soutenu une thèse intitulée "Troubles spécifiques du langage écrit et bilinguisme : proposition d'un modèle développemental de la lecture et de l'orthographe" en 2020, qui porte sur la structuration phonético-phonologique, la lecture et l'orthographe chez les enfants bilingues[^5].

Ses thématiques de recherche incluent la phonétique, la didactique de l'oral en FLE et les troubles spécifiques du langage écrit chez les enfants bilingues\[^2]\[^3][^4].

[^1] https://poezibao.typepad.com/poezibao/potes\_fiches\_biobilbiographiques/
[^2] https://lhumain.www.univ-montp3.fr/fr/les-membres/membres-de-lhumain/laurine-dalle
[^3] https://cv.hal.science/laurine-dalle?langChosen=fr
[^4] https://fr.linkedin.com/in/laurine-dalle-781043211
[^5] http://www.theses.fr/2020MON30060

## Projets
### En cours
Projet LISTIAC (Linguistically Sensitive Teachers in All Classrooms) (2019-2020)
Projet SIRIUS 2.0. (Policy Network on Migrant Education) (2017-2020)
### Passés
 Projet franco-canadien BINOGI « Plurilingual Pedagogies and Digital Technologies to Support Learning » financé par le CRSH (depuis 2020)
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Dalle](https://cv.hal.science/laurine-dalle?langChosen=fr)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/laurine-dalle-781043211?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAADWTrJEBVk1uPjyNHjtrAKNzV3NsnumuMKQ&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BaeI2QfD6TcW6WO54bdqDCw%3D%3D |
| ![[mail.svg\|20]]                           |  laurine.dalle@univ-montp3.fr    |
