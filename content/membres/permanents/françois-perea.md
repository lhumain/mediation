---
title: "François PEREA"
draft: false
tags: 
  - HU
  - M
  - I
  - N
date: ""
---

| Titre :  | PU                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 |
## Biographie
 François Perea est un linguiste et spécialiste de l'analyse du discours français. Il est professeur des universités en sciences du langage à l'Université Paul-Valéry Montpellier 3, où il est membre du laboratoire de recherche LHUMAIN.[^1][^3] 

Ses travaux portent notamment sur l'analyse des interactions, des usages numériques et des humanités numériques dans une approche anthropologique.[^2] Il s'intéresse particulièrement au langage corporel et aux relations entre corps et langage dans les interactions humaines et homme-machine.[^1][^2]

Parmi ses publications majeures, on peut citer "Des objets qui parlent ? Et ce qu'il reste aux humains" (2022), "Le dire et le jouir" (2017), "Langage et clinique de l'alcoolisme" (2006, avec J. Morenon) et "Paroles d'alcooliques" (2002).[^1] Il a également co-dirigé plusieurs ouvrages collectifs sur des thématiques liées au langage, au corps et aux technologies numériques.[^1][^2]
[^1] https://cv.hal.science/francois-perea
[^2] https://www.editionsmkf.com/portfolio-2/francois-perea/
[^3] https://lhumain.www.univ-montp3.fr/fr/perea-fran%C3%A7ois
[^4] https://www.amazon.fr/Fran%25C3%25A7ois-Perea/e/B004MT02YO%3Fref=dbs\_a\_mng\_rwt\_scns\_share

## Projets
### En cours
- [SCENOSCOPE](https://www.scenoscope.com/) avec [lucie-alidieres-dumonceaud|Lucie Alidières]
- Observer les expériences culturelles
- [NEXUS – Réussir en licence avec les Humanités numériques (PIA) :](https://www.univ-montp3.fr/fr/communiques/le-projet-nexus-officiellement-lanc%C3%A9-par-luniversit%C3%A9-paul-val%C3%A9ry-montpellier-3)  lauréat, porteur jusqu'en mars 2021.
- [HUT, Human At Home](http://www.hut-occitanie.eu/)  (HUT) : Membre du directoire du projet
### Passés
 - [SCENOSCOPE](https://www.scenoscope.com/)  - Observer les expériences culturelles
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Perea](https://cv.hal.science/francois-perea)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  |  |
| ![[mail.svg\|20]]                           |  françois.perea@univ-montp3.fr    |
