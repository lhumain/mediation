---
title: "Nathalie AUGER"
draft: false
tags: 
  - L
  - A
  - I
date: ""
---

| Titre :  | PU                        |
| -------- | ---------------------------------------- |
| Statut : | Directrice                       |
| Dates :  | depuis 2020 |
## Biographie
 Nathalie Auger est responsable du master de français langue seconde et de l'option FLE (Français Langue Étrangère) du CAPES Lettres dans cette même université.[⁵](http://glottopol.univ-rouen.fr/telecharger/numero\_11/gpl11\_complet.pdf) Ses recherches, menées en Europe et au Canada, portent sur l'enseignement du français "langue maternelle" dans les contextes plurilingues et multiculturels au 21e siècle.[¹](https://www.esf-scienceshumaines.fr/304\_\_auger-nathalie) [²](https://www.dunod.com/livres-nathalie-auger) Elle s'intéresse notamment aux questions de diversité linguistique et culturelle en éducation.[³](https://www.quoideneuf.ca/1136219/8281524) Nathalie Auger a grandi dans un quartier plurilingue de l'Est parisien, ce qui a orienté son parcours vers l'étude du plurilinguisme et de la mobilité.[³](https://www.quoideneuf.ca/1136219/8281524) Elle est l'auteure d'une dizaine d'ouvrages et de ressources en ligne sur ces sujets.[²](https://www.dunod.com/livres-nathalie-auger)

## Projets
### En cours
- **[LISTIAC (Linguistically Sensitive Teachers in All Classrooms)](https://lhumain.www.univ-montp3.fr/fr/programmes-europ%C3%A9ens-et-internationaux)** , porteur (2018-2021) : [https://listiac.univ-montp3.fr/](https://listiac.univ-montp3.fr/) 
- [**SIRIUS 2.0 – Policy Network on Migrant Education**](https://lhumain.www.univ-montp3.fr/fr/programmes-europ%C3%A9ens-et-internationaux) ; [site européen](https://lhumain.www.univ-montp3.fr/fr/programmes-europ%C3%A9ens-et-internationaux%20%20%20http://www.sirius-migrationeducation.org) : porteur (2017-2020) : [https://sirius.univ-montp3.fr/](https://sirius.univ-montp3.fr/) 
- **[UPVMultilingue,](https://lhumain.www.univ-montp3.fr/fr/autres-projets)** depuis 2018 : Création d’une application smartphone d’aide à la diffusion/compréhension de la signalétique et des informations disponibles dans l’UPVM pour les étudiants allophones : porteur (2018-2020)
### Passés
 - FLS France-Canada : « Pour une didactique interculturelle renouvelée du français langue seconde et vers une meilleure réussite scolaire des élèves allophones : 
	 une comparaison intersite France-Canada » (2016-2019)  financé par  le Conseil de Recherche en Sciences Humaines (CRSH Social Sciences and Humanities Research Council) : Développement de partenariat - Savoir, Didactique interculturelle, littérature de jeunesse, élèves allophones en France et au Canada.  Ce projet vise à comparer les pratiques d’enseignants œuvrant en milieux plurilingues et multiethniques au Canada (Ottawa) et en France (Montpellier), en contextes majoritaire (France) et minoritaire (Canada). Cette étude a pour but de recenser les points de convergence et de divergence au cœur des pratiques langagières pour proposer, dans une perspective interculturelle, une didactique du français langue seconde en cohérence avec les besoins grandissant des populations allophones.  Co-porteurs : Nathalie Auger, PU, UPVM, (France) et Carole Fleuret, PU, Université d’Ottawa (Canada).  Collaborateur: JL. Chiss, PU,  Université Sorbonne-Nouvelle, Paris 3,  Autres collaborateurs : 3 étudiants Masters et 3 doctorants.
- « Oralité, langues et imaginaire » chez les populations gitanes de Perpignan. Approches linguistiques et didactique ».  financé par le Fond Social Européen, Mairie et académie de Perpignan (évaluation, experts FSE en SL)  (2013-2016)  
	L’objectif est une meilleure compréhension des mécanismes d’apprentissages scolaires dans le contexte de plurilinguisme et d’oralité multiséculaire de cette population ainsi que la constitution d’outils pédagogiques. Elle intègre les questions de la langues familiales et scolaires.  Porteur : Nathalie Auger (PU) ;  Collaborateurs N. Matheu (doctorante), J. Sauvage (MCF-HDR).
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Auger](https://cv.hal.science/nathalie-auger?langChosen=fr)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/nathalie-auger-72837b217?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAADaxmVABRv4gdkWwqtynSRhVv8bf8W5jWWE&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3B%2BMXBPZdPTyuV1rVbzGnpRA%3D%3D |
| ![[mail.svg\|20]]                           |  nathalie.auger@univ-montp3.fr    |
