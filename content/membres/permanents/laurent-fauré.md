---
title: "Laurent FAURÉ"
draft: false
tags: 
  - L
  - HU
  - M
  - N
date: ""
---

| Titre :  | MCF                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 |
## Biographie
 Laurent Fauré est Maître de Conférences en sciences du langage à l'Université Paul Valéry - Montpellier 3, où il est membre du Laboratoire LHUMAIN (Langage, HUmanités, Média-tions, Apprentissage, Interaction, Numérique). [^1][^4][^5]

## Recherche

Ses domaines de recherche incluent :

\- L'analyse des pratiques discursives radiophoniques et les nouveaux enjeux médiatiques
\- L'étude des interjections et des vocalisations à l'oral
\- L'actualisation de l'intersubjectivité dans le discours
\- Les interactions verbales et l'incommunication
\- La transcription des données vocales et la notation des corrélats prosodiques[^3]

## Parcours académique  

Fauré a soutenu une thèse en 2000 à l'Université Paul Valéry, intitulée "L'interjection en français". Il a publié de nombreux articles sur les interjections, les vocalisations et l'analyse du discours radiophonique dans des revues comme Cahiers de praxématique et Corela.[^3]

Il a également co-dirigé un ouvrage collectif "Analyser la radio, Méthodes et mises en pratique" en 2016.[^3] Ses recherches récentes portent sur l'usage des dispositifs numériques pour l'évaluation des pratiques d'enseignement.[^3]

## Activités académiques

Fauré a participé à divers séminaires, journées d'études et colloques, présentant ses travaux sur des sujets comme le dialogisme dans le théâtre espagnol, l'implicite dans le discours, et l'aliénation discursive.[^2] Il a également été membre de jurys de validation des acquis de l'expérience et d'examens à l'Université de Toulon.[^2]

Citations:
[^1] https://univ-montp3.academia.edu/LaurentFaur%C3%A9
[^2] https://babel.univ-tln.fr/category/membres/membres-permanents/
[^3] https://scholar.google.fr/citations?hl=fr&user=-93QAgkAAAAJ
[^4] https://itic.www.univ-montp3.fr/fr/liste-des-enseignants-du-d%C3%A9partement-sciences-du-langage
[^5] https://lhumain.www.univ-montp3.fr/fr/faur%C3%A9-laurent

## Projets
### En cours
HUT « HUman at home ProjecT » (sous programmes : Walk@home, InterHUt : co-porteur avec D. Nourrit :  ce projet pluridisciplinaire  interroge l’habitat de demain. Il regroupe des scientifiques issus des organismes de recherche et des entreprises innovantes qui veulent comprendre et concevoir l’appartement de demain pour l’occupant du futur
OPALE « Observatoire pour l’amélioration du logement étudiant ». Dans le cadre de leur stratégie globale d’innovation en vue d’améliorer et de diversifier les formes du logement étudiant, les Crous s’associent avec l’université Paul Valéry de Montpellier, l’université de Nîmes et l’Ecole nationale d’architecture de Montpelier pour créer OPALE, l’Observatoire des pratiques et anticipations en logement étudiant. OPALE est un groupement d’intérêt scientifique (GIS) de dimension nationale, dont les premières expériences seront centrées sur le territoire languedocien afin d’éprouver les méthodes d’enquêtes et d’analyse : porteur
### Passés
 À compléter.
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Fauré](https://halshs.archives-ouvertes.fr/search/index/?qa%5BauthFullName_t%5D%5B%5D=FAUR%C3%89+LAURENT&submit_advanced=Rechercher&rows=30)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://fr.linkedin.com/in/laurent-faur%C3%A9-99159636 |
| ![[mail.svg\|20]]                           |  laurent.faure@univ-montp3.fr    |
