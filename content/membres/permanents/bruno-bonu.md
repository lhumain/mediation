---
title: "Bruno BONU"
draft: false
tags: 
  - HU
  - I
  - N
date: ""
---

| Titre :  | MCF                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2020 |
## Biographie
 Bruno Bonu est un enseignant-chercheur en sciences du langage à l'Université Paul Valéry Montpellier 3. Voici les principaux éléments de sa biographie:

\- Il occupe le poste de Maître de conférences en sciences du langage à l'Université Paul Valéry Montpellier 3[^1][^2][^4].

\- Il est membre du laboratoire LHUMAIN (Langages Humanités Médiations Apprentissages Interactions Numériques) de l'Institut des Technociences de l'Information et de la Communication (ITIC) de cette université[^2][^4].

\- Ses recherches portent notamment sur l'analyse des interactions pédagogiques médiées par les technologies numériques dans l'enseignement supérieur[^2][^3]. Il a travaillé sur le projet ENTICE qui étudiait l'intégration des environnements numériques de travail dans les pratiques universitaires[^2].

\- Il est responsable de l'axe "Interactions et environnements technologisés" au sein de son laboratoire depuis 2008[^5].

\- Parmi ses publications, on peut citer un chapitre sur l'analyse de conversation et la sociologie des usages dans l'ouvrage "Communiquer à l'ère numérique" aux Presses des Mines en 2011[^2][^5].

Bruno Bonu est donc un spécialiste reconnu des usages pédagogiques du numérique dans l'enseignement supérieur, avec une expertise particulière dans l'analyse des interactions en contexte technologique[^2][^3][^5].

Citations:
[^1] https://fr.linkedin.com/in/bruno-bonu-b38923199
[^2] https://cv.hal.science/bruno-bonu?langChosen=fr
[^3] https://theses.hal.science/tel-00873193/document
[^4] https://lhumaine.numerev.com/auteurs?auteur=2944
[^5] https://www.pressesdesmines.com/author-book/bonu-bruno/

## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/bruno-bonu-b38923199?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAC6WuJwBdjekuMc6I2hGSdnNrR-YixSeNHQ&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BaBnBaDaOQKS1l4PyS%2BB80Q%3D%3D |
| ![[mail.svg\|20]]                           |  bruno.bonu@univ-montp3.fr    |
