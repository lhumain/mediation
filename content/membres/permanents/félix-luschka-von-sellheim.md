---
title: "Félix LUSCHKA VON SELLHEIM"
draft: false
tags: 
  - HU
  - M
  - I
  - N
date: ""
---

| Titre :  | PRAG et Doctorant                        |
| -------- | ---------------------------------------- |
| Statut : | Membre                       |
| Dates :  | 2021 ? |
## Biographie
 Félix Luschka Von Sellheim est un professeur de lettres et chargé de mission TICE/numérique né en 1967. Il habite actuellement à Montpeyroux en France\[^1]\[^2][^3]. Voici les principales informations sur son parcours professionnel :

\- Professeur de Lettres dans l'Éducation Nationale depuis septembre 1999[^3].
\- Chargé de Mission TICE/Numérique au CRDP (Centre Régional de Documentation Pédagogique) de l'Académie de Montpellier en 2004[^3].
\- Administrateur de la webradio de l'Université Paul Valéry Montpellier 3[^3].
\- Actuellement PRAG/PRCE (Professeur Agrégé/Professeur Certifié) au Département des Sciences du Langage à l'Université Paul Valéry Montpellier 3\[^2]\[^3].

Ses thématiques de recherche incluent l'analyse de discours, les interactions et la sociolinguistique, la méthodologie ethnographique, la webradio et les événements éphémères[^5].

Citations:
[^1] https://copainsdavant.linternaute.com/p/felix-luschka-von-sellheim-1875332
[^2] https://lhumaine.numerev.com/comites?user=2752
[^3] https://fr.linkedin.com/in/f%C3%A9lix-luschka-41b95829
[^4] https://de.wikipedia.org/wiki/Felix\_von\_Sellheim\_Luschka
[^5] https://lhumain.www.univ-montp3.fr/fr/luschka-f%C3%A9lix

## Projets
### En cours
À compléter.
### Passés
 À compléter.
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/félix-luschka-41b95829?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAAYDiVcBaLEs84njAEVFV99Z13mIfWFDqY8&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3Bi8BQj2idQLiW4AlxeVpOvA%3D%3D |
| ![[mail.svg\|20]]                           |  felix.luschka@univ-montp3.fr    |
