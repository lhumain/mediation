---
title: "Zélie BOUSSOU"
draft: false
tags: 
date: ""
---

| Titre :  | Doctorant                        |
| -------- | ---------------------------------------- |
| Statut : | -                       |
| Dates :  | 2022 |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  |  |
| ![[mail.svg\|20]]                           |      |
