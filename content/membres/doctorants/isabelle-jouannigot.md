---
title: "Isabelle JOUANNIGOT"
draft: false
tags: 
date: ""
---

| Titre :  | Doctorant                        |
| -------- | ---------------------------------------- |
| Statut : | -                       |
| Dates :  | 2020 |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  |  |
| ![[mail.svg\|20]]                           |      |
