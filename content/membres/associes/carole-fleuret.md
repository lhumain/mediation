---
title: "Carole FLEURET"
draft: false
tags: 
date: ""
---

| Titre :  | PU                        |
| -------- | ---------------------------------------- |
| Statut : | Associé                       |
| Dates :  | 2021 ? |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/carole-fleuret-9a967031?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAAau1JEB-MiqwhLIy_vV9UpUZSXZTFZot8s&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3Br1638PkMSsiTLLm3h%2BFWOA%3D%3D |
| ![[mail.svg\|20]]                           |  cfleuret@uOttawa.ca    |
