---
title: "Frédéric MIQUEL"
draft: false
tags: 
date: ""
---

| Titre :  | IA-IPR                        |
| -------- | ---------------------------------------- |
| Statut : | Associé                       |
| Dates :  | 2021 ? |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/miquel-frederic-94265584?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAABHkxAIBIh2aF6juMVq1EKNFM0LIycyxY4k&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BO3kBo9F6TNOj4t0LYXgFHw%3D%3D |
| ![[mail.svg\|20]]                           |  frederic.miquel@ac-montpellier.fr     |
