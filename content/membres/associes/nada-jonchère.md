---
title: "Nada JONCHÈRE"
draft: false
tags: 
date: ""
---

| Titre :  | Ingénieur d'étude                        |
| -------- | ---------------------------------------- |
| Statut : | Associé                       |
| Dates :  | 2021 ? |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/nada-jonchere?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAACJFlJMBm5Um2VPim4ZOQ0y_rq4avPo6ZLk&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3B2cyuhZ57RFOCqVfOHlYVQA%3D%3D |
| ![[mail.svg\|20]]                           |  nada.jonchere@univ-montp3.fr    |
