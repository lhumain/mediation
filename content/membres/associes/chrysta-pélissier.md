---
title: "Chrysta PÉLISSIER"
draft: false
tags: 
  - A
  - M
  - N
date: ""
---

| Titre :  | MCF                        |
| -------- | ---------------------------------------- |
| Statut : | Associé                       |
| Dates :  | 2020 - 2022 |
## Biographie
 Chrysta Pélissier est maître de conférences habilitée à diriger des recherches (MCF HDR) en sciences du langage à l'Université Paul Valéry de Montpellier\[^2]\[^3][^5]. Ses travaux portent principalement sur les méthodologies de conception[^3]. Elle est co-auteure de l'ouvrage "Éthique, numérique et idéologies" publié en 2023 aux Presses des Mines, qui explore les implications éthiques des transformations numériques dans divers domaines sociaux, politiques et économiques[^1]. 

Pélissier est qualifiée aux fonctions de professeur des universités en sciences du langage et en sciences de l'éducation[^2]. Bien qu'enseignante-chercheuse à l'Université de Montpellier, peu d'autres détails biographiques sont fournis dans les résultats, qui se concentrent principalement sur ses publications et son affiliation universitaire.

Citations:
[^1] https://www.pressesdesmines.com/produit/ethique-numerique-et-ideologies/
[^2] https://www.univ-montp3.fr/fr/pelissier-chrysta
[^3] https://www.istegroup.com/fr/auteur/chrysta-pelissier/
[^4] https://shop.vivlio.com/author/chrysta-pelissier/233690
[^5] https://www.eyrolles.com/Accueil/Auteur/chrysta-pelissier-149279/

## Projets
### En cours
- Projet COMPER : Une approche par compétences pour le diagnostic, la régulation et la personnalisation de l’apprentissage,  financement ANR, 2018-24
- Projet Apprendre : De la formation initiale au dispositif de maintien/formation des enseignant.es au Rwanda, financement AUF, 2023-25
- Projet Groupe thématique numérique (GTnum), Direction du numérique pour l'éducation, financement Ministère de l'éducation nationale, 2023-26 
- Projet Hypermédia 2060 : évaluation croisée entre BUT , financement Université de Montpellier, université d'excellence i-site, 2023-24
- Projet Cordées de la réussite SOAN :  S'Orienter et s'Approprier le savoir avec le Numérique, financement Académie de Montpellier, 2020-24
### Passés
 À compléter.
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] | [Pélissier](https://cv.archives-ouvertes.fr/chrysta-pelissier)      |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  | https://www.linkedin.com/in/chrysta-pelissier-7502a44b?miniProfileUrn=urn%3Ali%3Afs_miniProfile%3AACoAAAqLESwBxA0Kk84WBdCosCi4MpE9oRPpgn0&lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3BT1LZXLeGThaNA%2FsjYBoNOA%3D%3D |
| ![[mail.svg\|20]]                           |  chrysta.pelissier@umontpellier.fr    |
