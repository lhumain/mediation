---
title: "Ailin NACUCCHINO"
draft: false
tags: 
date: ""
---

| Titre :  | ATER                        |
| -------- | ---------------------------------------- |
| Statut : | Membre non Perm                       |
| Dates :  | 2022-2023 |
## Biographie
 
## Projets
### En cours

### Passés
 
## Liens

| ![[HAL_logotype-rvb_fond-clair_fr.png\|85]] |       |
| ------------------------------------------- | -------------------- |
| ![[logo_linkedin.png\|60]]                  |  |
| ![[mail.svg\|20]]                           |  ailin.nacucchio@univ-orleans.fr    |
