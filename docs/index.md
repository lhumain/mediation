# Médiation

| Intitulé                 | Lien                                                                                                                                     |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------- |
| Consultation médiation   | https://lhumain.gitlab.io/mediation/                                                                                                     |
| Airtable                 | - [Édition membres](https://airtable.com/appBbEQRsupU6YM2B)<br>- [Lancer la génération](https://n8n.abbadie.ovh/webhook/airtable2quartz) |
| Procédures déjà en place | [Doc Clickup](https://doc.clickup.com/2620358/p/h/2fyy6-18052/a69f557a8fdd582)                                                           |


# Communication
## Charte graphique

| Intitulé                     | Lien                                                  |
| ---------------------------- | ----------------------------------------------------- |
| Document de charte graphique | [[Charte_graphique_LHUMAIN_V3.pdf\|Charte graphique]] |
| Logos                        | [[Tous les logos]]                                    |
| Docs à entête                | [[Tous les docs]]                                     |
| Polices                      | [[Polices]]                                           |

## Externe
### Réseaux sociaux

| Intitulé                | Lien                                                                                                                                                                                                                                                                    |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Planning éditorial      | Trello :<br>- [Lien de consultation](https://trello.com/b/C8VrheVB/planning-editorial-lhumain)<br>- [Lien d'invitation](https://trello.com/invite/b/6657409926305efeec8808b7/ATTIbf8ee0595cb5e3e5cfa3e541c0f7117eFE12ED36/planning-editorial-lhumain) (@univ-montp3.fr) |
| Génération publications | - [Lien tool v1](https://n8n.abbadie.ovh/form/58bd3157-263c-446f-805d-995a386b4e3b)<br>                                                                                                                                                                                 |
- [ ] tool v2 (procédure de curation & validation)
- [ ] #automatisations From Trello to LinkedIn
- [ ] #automatisations Curation RSS site LHUMAIN
### Emails

| Intitulé        | Lien                                                                                                                                                           |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Formulaires n8n | - ["Séminaire"](https://n8n.abbadie.ovh/form/lhumain_seminaires_mail_seminaire)<br>- ["Annonce"](https://n8n.abbadie.ovh/form/lhumain_seminaires_mail_annonce) |
