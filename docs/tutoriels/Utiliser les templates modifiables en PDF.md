Pour faciliter la communication sur les réseaux sociaux, nous livrons dans notre kit des templates.

Ils sont tous téléchargeables sur le planning éditorial.

Voici un tutoriel pour convertir ce PDF modifiable en image PNG prête à être diffusée sur les réseaux sociaux.

> [!todo] Pré-requis
> - [x] Un template modifiable
> - [x] Adobe Acrobat Reader (sa version gratuite suffit)

# I. Modifier les champs
1. Ouvrez le template avec Adobe Acrobat 
2. Identifiez les champs modifiables, le fond des champs apparaît en violet transparent.
3. Modifier le texte des champs, et si le template le permet, les images.
- [x] Vous obtenez dès maintenant le visuel, il ne reste qu'à l'exporter.

# II. Exportez le formulaire en "image"
1. Fichier → Enregistrer sous
2. En bas à gauche, une case à cocher "Convertir au format", **cochez cette case.**
3. À la suite de cette case, un menu déroulant pour choisir le format, **sélectionnez .PNG**
   ![[tuto-templates_rs-export.png]]
4. Choisissez ensuite le dossier où vous voulez exporter l'image, et donnez lui un nom.
> [!warning] Sur certains ordinateurs...
> Les choses ne se déroulent pas de la même façon. Il est possible que lorsque vous cliquez sur "Choisir un autre dossier", rien ne semble se déclencher.
> Alors votre fichier est passé en "conversion" tout en haut à droite de la fenêtre :
> ![[tuto-templates_rs-conversion.png]]
> Cliquez sur "Enregistrer-sous" pour continuer l'export où vous voulez.

