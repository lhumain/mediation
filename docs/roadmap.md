
> [!NOTE] STELLA
> ```tasks
> tag includes Stella
> ```


> [!NOTE] AXELLE
> ```tasks
> tag includes Axelle
> ```


> [!danger] LHUMAIN
> Ici les tâches qui doivent être réalisées par les membres de LHUMAIN
> ```tasks
> tag includes lhumain
> ```


> [!NOTE] Tâches trouvées ailleurs
> ```tasks
> not done
> path does not include docs/roadmap
> ```


# Communication interne
- [x] #lhumain Vérification des documents à en-tête ✅ 2024-10-18
- [ ] Emails
	- [x] #Axelle Vérifier les formulaires ✅ 2024-10-15
	- [x] #Axelle Formulaire colloque ✅ 2024-10-15
- [?] Deck de présentation
# Communication externe
- [/] Templates
	- [x] #Stella Ajouter les logos (sous forme de champs formulaire) ✅ 2024-10-10
	- [ ] #Axelle Formation utilisation des templates (voir [[Utiliser les templates modifiables en PDF]] et transposer sur Moodle)
- [ ] Planning éditorial
	- [x] Conception ✅ 2024-10-02
	- [x] Ajout des documents ✅ 2024-10-02
	- [ ] #lhumain Connecter avec d'autres outils
	- [ ] #Axelle Formation utilisation du planning éditorial sur Trello
- [ ] Réseaux sociaux
	- [?] #lhumain Créer LinkedIn
- [ ] Affiche séminaires
	- [-] #Stella #Axelle Figer un template 📅 2024-10-11 ❌ 2024-10-18
	- [ ] Gérer la question de l'image de fond
	- [-] #Stella Formation utilisation du formulaire pour faire une affiche ❌ 2024-10-18
- [ ] Signature email
	- [x] #Axelle Refaire le tuto ✅ 2024-10-18
	- [x] #Axelle Formation signature ✅ 2024-10-18
# Médiation
- [?] #lhumain Quelle suite pour la médiation ?
- [ ] #lhumain Hébergement final
- [ ] #Axelle Procédure pour alimenter
- [ ] #Axelle Connecter à Hypothes.is
- [ ] #lhumain Manifeste animé
# Formations
*Non notées par ailleurs*
- [x] #Axelle Formulaires email ✅ 2024-10-17
- [ ] #Axelle Formation planning éditorial
- [ ] #Stella Rédiger une publication